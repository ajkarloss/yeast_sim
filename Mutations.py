from __future__ import division
import random
import sys
import math
import numpy
import scipy

"""
    Import user defined Modules
"""
import Sub_Functions as SF
import Yeast_Simulator as YS

cell_division_max = YS.cell_division_max
cell_division_min = YS.cell_division_min

def introduce_mutation(mother_cell,daughter_cell,beneficial_mutation_rate,k,tp,n_mut,PM,PM_fitness,FAMR,mutation_fitness):
    """
    Introducing Mutations Based on Yeast Mutation rate
    
    This function decides whether the mutation is fitness affecting or neutral.
    If the mutation is fitness affecting, whether its beneficial or deleterious.
    Whether mother cell or daughter cell will get the mutation
    
    And, it decides whether the mother or daughter cell should get the mutation.
    
    Input:
        mother_cell - Mother and daughter cell
        daughter_cell - Proportions of fitness affecting mutations
        beneficial_mutation_rate - Proportions of beneficial mutations.
        fitness - Fitness effects
        n_mut - Number of mutations
        k - Id of the daughter cell
        tp - Id of the daughter cell
        FAMR - Fitness affecting mutation rate 
        
    Output:
        Mother or daughter cell with new cell division time if the mutation is fitness affecting mutation
        mother_cell - Mother cell with extra mutations and changed mutations
        daughter_cell - daughter cell with extra mutations and changed mutations
        PM_fitness - fitness id and the fitness value
            PM_fitness[n_mut] = 25
        PM - cell id and the mutations id
            PM[k] = n_mut
            PM[tp] = n_mut
    """
    
    beneficial = 0
    deleterious = 0
    fitness_proportion = 0
    
    """ Decide whether its fitness altering mutation or neutral. """
    if random.randrange(100) < FAMR:
        """ Decide Whether its Adavantageous or Disadvantageous or neutral mutation. """
        if random.random() <= beneficial_mutation_rate:
            beneficial = 1
        else:
            deleterious = 1
    
    """ fix_cal decides whether GDs carry any fitness effects. """
    if random.randrange(100) < FAMR:        
        fitness = mutation_fitness[n_mut]
    else:
        fitness = 0 
    
    """
        Choose the cell:Parent or child to have mutation.
    """
    if random.random() <= 0.5:
        """ Assigning the Chromosome and the position. """
        if fitness != 0:
            mother_cell[1] = SF.calculate_cell_division_time(mother_cell[1],fitness,cell_division_max,cell_division_min)
            
        PM_fitness[n_mut] = fitness
        mother_cell[3] += 1
        
        """ Appending the mutation number """
        PM[k].append(n_mut)
        
    else:
        if fitness != 0:
            daughter_cell[1] = SF.calculate_cell_division_time(daughter_cell[1],fitness,cell_division_max,cell_division_min)
        
        PM_fitness[n_mut] = fitness        
        daughter_cell[3] +=  1
       
        """ Appending the mutation number """
        PM[tp].append(n_mut)
    
    return mother_cell,daughter_cell,PM,PM_fitness