"""
    This module contains the common subroutines for the Yeast Expereimtnal Simulator.
""" 

from __future__ import division
from random import *
import sys
import operator
from copy import copy
from copy import deepcopy
import math
import re
import numpy as np
from heapq import heapify
from numpy import *

"""
    Importing user defined modules.
"""
#import Mutations

def initialize_population(n_individuals,n_generations,cell_division_min,cell_division_max,cell_max_age):
    """
    Intialization 
    Intialization of the individual cells before the cell division starts.
    This part of initialization is for "Rate only simulations"
    
    Input:
        n_individuals - number of individuals
        n_generations - number of generations
        cell_division_min - cell division time of WT cells under normal conditions 
        cell_division_max - cell division time of WT cells under stress conditions 
        cell_max_age - maximum age of the cells. A cell cant divide indefinitely.
        
    Output:
        Dictionary of lists with cell division time, mutation and age properties
        e.g. genotypes[n] = [cell_division_max,cell_division_max,cell_max_age,number of mutations]
            genotype[1] = [180,180,16,0]
            PM[1] = []
    """
    
    size = n_individuals * 2**n_generations
    genotypes = ones((size,4),dtype=int32)
    
    for n in xrange(n_individuals):
        genotypes[n] = [cell_division_max,cell_division_max,cell_max_age,0]
    
    """ Making the Point mutation and gene duplication Dict of list """
    PM = {}; 
    for i in range(size):
        PM[i] = []
        
    return genotypes,PM

def initialize_population_lag(n_individuals,n_generations,rate_time_WT_stress,rate_time_Mut_stress,lag_time_WT_stress,lag_time_Mut_stress,cell_max_age):
    """
    Intialization of the individuals before the cell division starts.
    This part of initialization is for "Competition Assay with LAG AND RATE Model"
    
    Input:
        n_individuals - number of individuals
        n_generations - number of generations
        cell_division_min - cell division time of WT cells under normal conditions 
        cell_division_max - cell division time of WT cells under stress conditions 
        cell_max_age - maximum age of the cells. A cell cant divide indefinitely.
    
    Output:
        Dictionary of lists with cell division time, mutation and age properties
        e.g. genotypes[n] = [cell_division_max,cell_division_max,cell_max_age,number of mutations]
            genotype[1] = [805,805,16,0]
            PM[1] = []
    """
    
    size = n_individuals * 2**n_generations
    genotypes = ones((size,4),dtype=int32)
    
    """
        CRT - Cell divsion time for a cell during Rate phase
        CLT - The Amount of time a cell spends in Lag phase before it starts to divide(Rate phase)
    """
    CRT = {}
    CLT = {}
    
    for n in xrange(n_individuals):
        genotypes[n] = [lag_time_WT_stress,lag_time_WT_stress,cell_max_age,0]
        
        CRT[n] = rate_time_WT_stress
        CLT[n] = lag_time_WT_stress
        
    """ Making the Point mutation and gene duplication Dict of list """
    PM = {}; 
    for i in range(size):
        PM[i] = []
    
    """ Deciding the mutant cell """
    import random
    N = random.randrange(1,n_individuals)
    
    CRT[N] = rate_time_Mut_stress
    CLT[N] = lag_time_Mut_stress
    
    genotypes[N][0] = lag_time_Mut_stress
    genotypes[N][0] = lag_time_Mut_stress
    genotypes[N][3] = 2
    
    return genotypes,PM,CRT,CLT

def initialize_population_target_mutations(n_individuals,n_generations,WT_Lag,WT_Rate,cell_max_age):
    """
    Intialization 
    Intialization of the individuals before the cell division starts.
    This part of initialization is for Competition Assay with LAG AND RATE Model
    
    Input:
        n_individuals - number of individuals
        n_generations - number of generations
        cell_division_min - cell division time of WT cells under normal conditions 
        cell_division_max - cell division time of WT cells under stress conditions 
        cell_max_age - maximum age of the cells. A cell cant divide indefinitely.
    
    Output:
        Dictionary of lists with basic properties
        e.g. genotypes[n] = [cell_division_max,cell_division_max,cell_max_age,number of mutations]
            genotype[1] = [805,805,16,0]
            PM[1] = []
    """
    size = n_individuals * 2**n_generations
    genotypes = ones((size,4),dtype=int32)
    
    """
        CRT - Cell divsion time for a cell during Rate phase
        CLT - The Amount of time a cell spends in Lag phase before it starts to divide(Rate phase)
    """
    
    CRT = {}
    CLT = {}
    
    for n in xrange(n_individuals):
        genotypes[n] = [WT_Lag,WT_Rate,cell_max_age,0]
        
        CRT[n] = WT_Rate
        CLT[n] = WT_Lag
        
    # Making the Point mutation and gene duplication Dict of list 
    PM = {}; 
    for i in range(size):
        PM[i] = []
    
    return genotypes,PM,CRT,CLT

def inactivate_cells(genotypes,percentage):
    """
    NOT IN USE: WILL BE DELETED ONCE CHECKED
    
    In Reality 10% of the cells wont survive or wont reproduce. This function will do the killings.
    
    Input:
        dictionary of lists (cells)
        % cells to be dead or wont survive
        
    Output:
        dictionary of lists (cells) with inavtivated cells
    """
    
    inactive_ids = {}
    n_inactive_cells = int(len(genotypes) * (percentage/100))
    inactive_ids = random.sample(genotypes,n_inactive_cells)    
    
    for i in range(0,len(inactive_ids)):
        id = inactive_ids[i]
        genotypes[id][2] = 0
    
    return genotypes

def group_cells(genotypes,n_individuals):
    """
    Group the cells based on their cell division time.
    So the cells can divide at the right time
    
    Input:
        genotypes - dictionary of lists (cells)
        n_individuals - Number of individuals
    
    Output:
        grouped cells
        E.g.
            cell_group[180] = [180,180,0,16,0]
    """
    cell_group = {}
    n_individuals = int(n_individuals)
    for i in xrange(n_individuals):
        if cell_group.has_key(genotypes[i][0]):
            cell_group[genotypes[i][0]].append(i) 
        else:
            cell_group[genotypes[i][0]] = [i]
            
    return cell_group

def calculate_mean_cell_division_time(genotypes,output_file,i):
    """
    NOT IN USE: WILL BE DELETED ONCE CHECKED
    
    This is to calculate the mean cell divison time of the populations at each bottlneck.
    Input:
        genotypes - Dictionary of lists with cell division time
        
    Output:
        Mean cell division time of the population
    
    i is the bottlneck number
    """
    
    total_time = 0
    n_cells = 0
    for l in xrange(0,len(genotypes)):
        """  Ignore the dead cells. """
        if genotypes[l][2] != 0:
            total_time = total_time + genotypes[l][1]
            n_cells = n_cells + 1
    mean_division_time = total_time/n_cells
    
    print i,"\t",mean_division_time
    
    return mean_division_time

def assign_haplotype(n):
    """
    Assigning the Haplotype structure.
    This funtion will choose the chomosome number and
    the position for every mutation.
    
    Input:
        N - Number or ID to create haplotypes e.g. nth haplotype
    
    Output:
        haplotype_struture[n] = "5:343434"
        
    """
    """ Chromosome numbers and their length."""
    chrom = {
             1:230208,2:813178,3:316617,4:1531918,
             5:576869,6:270148,7:1090946,8:562643,
             9:439885,10:745745,11:666454,12:1078175,
             13:924429,14:784333,15:1091289,16:948062
             }

    haplotype_structure = []
    
    for i in range(n):
        """ Choose a chromosome randomly"""
        chr_no = random.randint(1,16)

        """  Choose a specific position in chromosome randomly """
        bp_position = random.randint(1,chrom[chr_no])
        haplotype_structure.append(str(chr_no) + str(":") + str(bp_position))
        
    return haplotype_structure

def make_fitness_proportions(fitness,MP):
    """
    Convert the fitness values to beneficial and deleterious bases on their proportions
    See Materials and Methods in Jeevan et al. 2016

    Input:
        fitness - List cell division times
        MP - Proportion of beneficial mutations in percentage
    
    Output:
        fitness - List of fitness values converted to beneficial or deleterious
    """
    import random as r
    n1 = int(len(fitness) * (100-MP)/100)
    n2 = len(fitness)
    pop = xrange(n2)
    sampled = r.sample(pop,n1)
    for i in sampled:
        fitness[i] *= -1

    return fitness

def convert_to_minutes(fitness,rate_WT_stress,rate_Mut_stress,truncation):
    """
    Convert the fitness values to minutes from random gamma values
    See Materials and Methods in Jeevan et. al 2016.
    
    Input:
        fitness - List of random values from gamma distribution
    
    Output:
        fitness_mins - gamma distribution values are converted to mins
    """
    fitness_mins = []
    for i in fitness:
        fitness_mins.append(calculate_relative_cell_division_time(i,rate_WT_stress,rate_Mut_stress,truncation))
    
    return fitness_mins


def calculate_relative_cell_division_time(fitness,rate_WT_stress,rate_Mut_stress,truncation):
    """
    Input:
        fitness - one random value from Gamma distribution
        rate_WT_stress - rate values for WT cell in stress
        rate_Mut_stress - rate values for Mutant cell in stress
        truncation - higher limit for the change in the cell division time
        
    Output:
        changed_CDT - converted value
    """
    #cell_division_max = 180
    #cell_division_min = 90
    #change_CDT = (cell_division_max-cell_division_min) * alpha
    
    #change_CDT = (fitness/(1+fitness)) *  cell_division_max
    change_CDT = (fitness/(1+fitness)) *  rate_WT_stress

    if change_CDT > truncation:
        change_CDT = truncation-1
        
    return change_CDT


def truncate_fitness_effects(fitness,trunc):
    """
    Values bigger than the difference between WT and Mutatant cant be included due to the design of our model
    Input:
        fitness - List of fitness values
        trunc - maximum limit for the truncation 
    Output:
        new_fitness - Truncated vfitness alues
        e.g. all the values >89 will be removed
    """
    
    new_fitness = []
    for i in fitness:
        if i <= trunc:
            new_fitness.append(i)
    
    return new_fitness

def calculate_cell_division_time(current_CDT,alpha,cell_division_max,cell_division_min):
    """
    Calculating new cell division times based on current cell division time and fitness
    See Materials and Methods in Jeevan et. al 2016.
    
    Input:
        current_CDT - current cell division time of a cell
        alpha - Fitness effect of the new mutation
    
    Output:
        new_CDT - New cell division time calculated based on exisiting cell division time
    """
    
    #cell_division_max = 180
    #cell_division_min = 90
    
    CDT_change = 0
    if alpha > 0:
        CDT_change = (current_CDT - cell_division_min)/(cell_division_max - cell_division_min)
        CDT_change *= alpha
        new_CDT = current_CDT - CDT_change
    else:
        new_CDT = current_CDT + abs(alpha)

    return  new_CDT

def read_target_information():
    """
    See Materials and Methods in Jeevan et. al 2016.
        2.2 Targetted Mutation simulations
        Read taget file for target name, size, rate and lag values
        for the cell if a cell gets a mutation.
    Input:
        "Target.txt" is the input file which contains the data
        
    Output
        target_rate - Rate values (in mins) for mutations
            e.g. target_rate["ABC"] = 120
                 target_rate["BCA"] = 135
        target_lag - Lag values (in mins) for mutations
            e.g. target_lag["ABC"] = 440
                 target_lag["BCA"] = 340
        target_size - Size of each mutation targets in basepairs
            e.g. target_lag["ABC"] = 600
                 target_lag["BCA"] = 750        
        target_range - Ranges of the mutations in genome
            e.g. target_lag["ABC"] = "1-600"
                 target_lag["BCA"] = "601-1350"
    """
    target_rate = {};target_lag = {}
    target_size = {};target_range = {}
    F1 = open("Target.txt","r")
    N = 0
    for i in F1:
        temp = i.split()
        target_rate[temp[0]] = int(temp[2])
        target_lag[temp[0]] = int(temp[1])
        target_size[temp[0]] = int(temp[3])
        if N == 0:
            target_range[temp[0]] = [0,int(temp[3])]
            s = int(temp[3]) + 1
        else:
            target_range[temp[0]] = [s,int(temp[3])]
            s = int(temp[3]) + 1
            
        N += 1
    
    return target_rate,target_lag,target_size,target_range