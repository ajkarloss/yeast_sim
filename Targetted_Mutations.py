from __future__ import division
import random
import sys
import math
import numpy
import scipy

###################################################################################################
############################### Import user defined Modules. ######################################
################################################################################################### 
import Sub_Functions as SF
###################################################################################################
################### Introducing Mutations Based on Yeast Mutation rate. ###########################
###################################################################################################
def introduce_mutation(mother_cell,daughter_cell,beneficial_mutation_rate,k,tp,n_mut,PM,PM_fitness,target_rate,target_lag,target_size,target_range,CRT,CLT,CH,N_CH,MID_Hap):
    """
    This function decides whether the mutation is fitness affecting or neutral.
    If the mutation is fitness affecting, whether its beneficial or deleterious.
    Whether mother cell or daughter cell will get the mutation
    
    And, decides whether the mother or daughter cell should get the mutation.
    
    Input:
        Mother and daughter cell
        Proportions of fitness affecting mutations
        Proportions of beneficial mutations.
        Fitness effects
        Number of mutations
    
    Output:
        Mother or daughter cell with extra mutations
        Mother or daughter cell with new cell division time if the mutation is fitness affecting mutation
    """
    hap = 0
    ######################################################
    # Choose the cell:Parent or child to have mutation.
    ######################################################
    if random.random() <= 0.5:
        n_mut += 1
        # Find where the mutation falls
        hap = find_haplotype(target_range)
        if hap != 0:
            if mother_cell[1] >= target_rate[hap]:
                mother_cell[1] = target_rate[hap]
                CRT[k] = target_rate[hap]
            
            if CLT[k] >= target_lag[hap]:
                CLT[k] = target_lag[hap]
            
            # Storing Genotypes and Haplotypes
            if CH.has_key(k):
                if CH[k] == "W":
                    CH[k] = hap
                    #N_CH[k] = N_HAP1 + N_HAP2 + N_HAP3
                else:
                    CH[k] = CH[k] + hap
                    #N_CH[k] = N_HAP1 + N_HAP2 + N_HAP3
            else:
                CH[k] = hap
                #N_CH[k] = N_HAP1 + N_HAP2 + N_HAP3
                
            MID_Hap[n_mut] = hap
            
            PM[k].append(n_mut)
    else:
        n_mut += 1
        # Find where the mutation falls
        hap = find_haplotype(target_range)
        if hap != 0:
            if daughter_cell[1] >= target_rate[hap]:
                daughter_cell[1] = target_rate[hap]
                CRT[tp] = target_rate[hap]
                
            if CLT[tp] >= target_lag[hap]:
                CLT[tp] = target_lag[hap]
            
            # Storing Genotypes and Haplotypes
            if CH.has_key(tp):
                if CH[tp] == "W":
                    CH[tp] = hap
                    #N_CH[k] = N_HAP1 + N_HAP2 + N_HAP3
                else:
                    CH[tp] = CH[tp] + hap
                    #N_CH[tp] = N_HAP1 + N_HAP2 + N_HAP3
            else:
                CH[tp] = hap
                #N_CH[k] = N_HAP1 + N_HAP2 + N_HAP3
            
            PM[tp].append(n_mut)
            MID_Hap[n_mut] = hap
            
    return mother_cell,daughter_cell,PM,PM_fitness,CRT,CLT,N_CH,CH,MID_Hap

################################################################################################
# Check where the mutation falls: If the mutation falls inside the target then introduce
################################################################################################
def find_haplotype(ranges):
    hap = 0
    bps = random.randint(1,12070899)
    for i in ranges:
        temp = ranges[i]
        if bps >= int(temp[0]) and bps <= int(temp[1]):
            hap = i
            break

    return hap